package ma.codeheart.apps.shared;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Component
public class Utils {
	
	private final Random RANDOM = new SecureRandom();
	private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	
	
	public String generateUserId(int length) {
		return generateRandomString(length);
	}

	private String generateRandomString(int length) {
		StringBuilder returnValue = new StringBuilder(length);
		
		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}
		
		return new String(returnValue);
	}
	
	public static boolean checkLogin(String claimer) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		for(GrantedAuthority au: auth.getAuthorities()) {
			if(au.toString().contains("ADMIN")) {
				return true;
			}
		}
		
		
		String username = auth.getPrincipal().toString();
		if(claimer.equals(username)) {
			return true;
		} else {
			return false;
		}
	}
	
}
