package ma.codeheart.apps.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.codeheart.apps.exceptions.UserServiceException;
import ma.codeheart.apps.request.UserDetailsRequestModel;
import ma.codeheart.apps.response.ErrorMessages;
import ma.codeheart.apps.response.UserRest;
import ma.codeheart.apps.service.UserService;
import ma.codeheart.apps.shared.dto.UserDto;


@RestController
@RequestMapping("users")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping(path = "/{id}")
	public UserRest getUser(@PathVariable String id) {
		 
		 UserRest returnValue = new UserRest();
		UserDto userDto = userService.getUserByUserId(id);
		BeanUtils.copyProperties(userDto, returnValue);
		 
		return returnValue;
	}
	
	@PostMapping
	public UserRest createUser(@RequestBody UserDetailsRequestModel userDetails) {
		
		UserRest returnValue = new UserRest();
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);
		if(userDetails.getFirstName().isEmpty()) throw new UserServiceException(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());
		UserDto createadUser = userService.createUser(userDto);
		BeanUtils.copyProperties(createadUser, returnValue);

		return returnValue;
	}
	
	@PutMapping(path = "/{id}")
	public UserRest updateUser(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetails) {
		UserRest returnValue = new UserRest();
		
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);
		
		UserDto modifiedUser = userService.updateUser(id, userDto);
		BeanUtils.copyProperties(modifiedUser, returnValue);

		return returnValue;
	}
	
	@DeleteMapping
	public String deleteUser() {
		return "user was deleted";
	}
}

