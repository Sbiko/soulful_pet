package ma.codeheart.apps.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ma.codeheart.apps.entities.UserEntity;


@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long>{
		UserEntity findByEmail(String email);
		UserEntity findByUserId(String userId);
}
