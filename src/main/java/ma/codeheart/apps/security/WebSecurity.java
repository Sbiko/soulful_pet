package ma.codeheart.apps.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ma.codeheart.apps.dao.UserRepository;
import ma.codeheart.apps.service.UserService;



@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{

	private final UserService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private UserRepository userRepository;
	
	public WebSecurity(UserService userDetailsService,BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	//	http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.GET,"/users/xd").hasRole("MEMBRE").antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL)
	//	.and()
		//.addFilter(getAuthenticationFilter())
		//.addFilter(new AuthorizationFilter(authenticationManager()))
		//.permitAll().anyRequest().authenticated().and()
	//	.sessionManagement()
       // .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http
        .cors()
        .and()
        // remove csrf and state in session because in jwt we do not need them
        .csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        // add jwt filters (1. authentication, 2. authorization)
        .addFilter(getAuthenticationFilter())
        .addFilter(new AuthorizationFilter(authenticationManager(), this.userRepository))
        .authorizeRequests()
        // configure access rules
        .antMatchers(HttpMethod.POST, "/users").permitAll()
        //.antMatchers("/users/xd").hasRole("MEMBRE")
        .anyRequest().authenticated();
		
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	public AuthenticationFilter getAuthenticationFilter() throws Exception {
		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());
		filter.setFilterProcessesUrl("/users/sign_in");
		return filter;
	}
	
	
}
