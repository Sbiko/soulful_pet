package ma.codeheart.apps.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import ma.codeheart.apps.shared.dto.UserDto;


public interface UserService extends UserDetailsService{
	UserDto createUser(UserDto user);
	UserDto getUser(String email);
	UserDto getUserByUserId(String userId);
	UserDto updateUser(String userId, UserDto userDto);
}
