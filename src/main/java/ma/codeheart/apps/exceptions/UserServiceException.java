package ma.codeheart.apps.exceptions;

public class UserServiceException extends RuntimeException{

	private static final long serialVersionUID = -6041433688123928527L;
	
	public UserServiceException(String message) {
		super(message);
	}
	
}
